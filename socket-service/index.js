const socketIO = require("socket.io");
const http = require("http");
const redisSocketIO = require("socket.io-redis");
let socketServer = http.createServer();
socketServer.listen(9000, "0.0.0.0");
let io = socketIO(socketServer);
io.adapter(redisSocketIO({ host: "redis_1", port: 6379 }));
io.on("connection", (socket) => {
    console.log('hello')
});
