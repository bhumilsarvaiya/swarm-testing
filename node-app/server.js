let express = require('express')
let app = express()
let bodyParser = require('body-parser')
let multer = require('multer')
let upload = multer()
let number = Math.random() * 10000
let cassandra = require('cassandra-driver')
let client = new cassandra.Client({ contactPoints: ['cassandra_1', 'cassandra_2'], policies: {
    loadBalancing: new cassandra.policies.loadBalancing.RoundRobinPolicy(),
    reconnection: new cassandra.policies.reconnection.ConstantReconnectionPolicy(500)
} });
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
let counter = 3000;
let init = async () => {
    try {
        await client.execute(`CREATE KEYSPACE "qually" WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 2}`, [])
        await client.execute(`CREATE TABLE qually.user (
            fake_key TEXT,
            first_name VARINT,
            num VARINT,
            PRIMARY KEY(fake_key, first_name)
        )`, [])
        await client.execute(`INSERT INTO qually.user (fake_key, first_name, num) VALUES (?, ?, ?)`, ['hey', counter, counter], {prepare: true})
        counter += 1
        console.log('keyspaces, table created')
    } catch (e) {
        console.log(e.message)
    }
}

init()

app.get('/', async (req, res) => {
    try {
        console.log('begin')
        await client.execute(`INSERT INTO qually.user (fake_key, first_name, num) VALUES (?, ?, ?)`, ['hey', counter, counter], {prepare: true})
        counter += 1
        console.log('inserted')
        let data = await client.execute("SELECT first_name FROM qually.user WHERE fake_key = 'hey' ORDER BY first_name", []);
        console.log('fetched')
        let result = []
        for (let i=0; i<data.rows.length; i++) {
            result.push(Number(data.rows[i].get("first_name")))
        }
        console.log('sending')
        res.send(number + '   ' + result);
    } catch (err) {
        res.send(err.message);
    }
    // res.sendFile(__dirname + "/index.html")
})

app.get('/hello/:surname', async (req, res) => {
    res.send(number + ' hello  ' + req.query.name + req.params.surname);
    // res.sendFile(__dirname + "/index.html")
})

app.post('/hello', upload.array(), function (req, res) {
  res.json(req.body);
});
app.listen(9000)
